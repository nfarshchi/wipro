//
//  NetworkManager.swift
//  WiproTechTest
//
//  Created by Navid Farshchi on 20/5/18.
//  Copyright © 2018 Navid Farshchi. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

protocol NetworkDelegate {
    func onSuccessWithResponse(response : JSON)
    func serviceCallOnProgress()
    func serviceCallFinished()
}

func getRequest(server: String, request: String, delegate : NetworkDelegate) {
    delegate.serviceCallOnProgress()
    Alamofire.request(server + request).responseString { response in
        if let jsonString = response.result.value {
            if let json: AnyObject = jsonString.parseJSONString {
                let swiftJson = JSON(json)
                print(swiftJson["title"].stringValue)
                delegate.onSuccessWithResponse(response: swiftJson)
                delegate.serviceCallFinished()
            }
        }
    }
}

extension String {
    
    var parseJSONString: AnyObject? {
        
        let data = self.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            if let jsonData = data {
                // Will return an object or nil if JSON decoding fails
                return try JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.mutableContainers) as AnyObject
            } else {
                // Lossless conversion of the string was not possible
                return nil
            }
        } catch {
            return nil
        }
    }
}
