//
//  DataManager.swift
//  WiproTechTest
//
//  Created by Navid Farshchi on 20/5/18.
//  Copyright © 2018 Navid Farshchi. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol DataManagerDelegate {
    func onDataReady(_ data : [FactObject])
    func onNewTitleString(_ titleString : String)
    func onDataReadingInProgress()
    func onDataReadingFinished()
}

class DataManager : NetworkDelegate {
    
    private init() { }
    
    static let shared = DataManager()
    
    var dataArray : [FactObject] = []
    var titleString : String = ""
    var dataDelegate : DataManagerDelegate!
    
    
    func registerDataDelegate(_ delegate : DataManagerDelegate) {
        self.dataDelegate = delegate
    }
    
    func readData() {
        getRequest(server: Constants.server.serverURL, request: Constants.server.serviceAPIURL, delegate: self)
    }
    
    //MARK: Network Delegate Methods
    func onSuccessWithResponse(response: JSON) {
        dataDelegate.onNewTitleString(response["title"].stringValue)
        dataArray.removeAll()
        for i in 0..<response["rows"].arrayValue.count {
            let fact = FactObject()
            fact.titleString = response["rows"][i]["title"].stringValue
            fact.descriptionString = response["rows"][i]["description"].stringValue
            fact.imageHerfString = response["rows"][i]["imageHref"].stringValue
            if !(fact.titleString == "" && fact.descriptionString == "" && fact.imageHerfString == "") {
                dataArray.append(fact)
            }
        }
        dataDelegate.onDataReady(dataArray)
    }
    
    func onFailedWithError(error: Error) {
        print(error)
    }
    
    func serviceCallOnProgress() {
        dataDelegate.onDataReadingInProgress()
    }
    
    func serviceCallFinished() {
        dataDelegate.onDataReadingFinished()
    }
}

