//
//  MainController.swift
//  WiproTechTest
//
//  Created by Navid Farshchi on 20/5/18.
//  Copyright © 2018 Navid Farshchi. All rights reserved.
//

import UIKit

class MainController : UICollectionViewController, UICollectionViewDelegateFlowLayout, DataManagerDelegate {
    
    var factsArray : [FactObject] = []
    let cellID = "cellIdentifier"
    var refreshControl : UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.backgroundColor = .white
        collectionView?.register(FactsCollectionViewCell.self, forCellWithReuseIdentifier: cellID)
        
        self.refreshControl = UIRefreshControl()
        collectionView?.alwaysBounceVertical = true
        self.refreshControl.tintColor = UIColor.red
        self.refreshControl.addTarget(self, action: #selector(loadData), for: .valueChanged)
        collectionView?.addSubview(refreshControl)
        
        DataManager.shared.registerDataDelegate(self)
        DataManager.shared.readData()
    }
    
    @objc func loadData() {
        DataManager.shared.readData()
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //        return factsArray.count
        //Temp test
        return factsArray.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as! FactsCollectionViewCell
        cell.fillWithData(factsArray[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if let fact = factsArray[indexPath.item] as? FactObject {
            let estimatedDescriptionLabelViewWidth = view.frame.width - 16
            let size = CGSize(width: estimatedDescriptionLabelViewWidth, height: 1000)
            
            let attributes = [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 12)]
            
            let estimatedFrame = NSString(string: fact.descriptionString).boundingRect(with: size, options: .usesLineFragmentOrigin, attributes: attributes, context: nil)
            
            var height = estimatedFrame.height + 90
            if fact.imageHerfString != "" {
                height += 100
            }
            
            return CGSize(width: view.frame.width, height: height)
        }
        return CGSize(width: view.frame.width, height: 300)
    }
    //MARK: - DataManager Delegate
    func onDataReady(_ data: [FactObject]) {
        self.factsArray = data
        DispatchQueue.main.async {
            self.collectionView?.reloadData()
        }
    }
    
    func onNewTitleString(_ titleString: String) {
        DispatchQueue.main.async {
            self.navigationItem.title = titleString
        }
    }
    
    func onDataReadingInProgress() {
        
    }
    
    func onDataReadingFinished() {
        self.refreshControl.endRefreshing()
    }
}

