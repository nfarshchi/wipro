//
//  FactsObject.swift
//  WiproTechTest
//
//  Created by Navid Farshchi on 20/5/18.
//  Copyright © 2018 Navid Farshchi. All rights reserved.
//

import Foundation

class FactObject : NSObject { 
    var titleString : String = ""
    var descriptionString : String = ""
    var imageHerfString : String = ""
    var imageData : Data? = nil
}
