//
//  FactsTableViewCell.swift
//  WiproTechTest
//
//  Created by Navid Farshchi on 20/5/18.
//  Copyright © 2018 Navid Farshchi. All rights reserved.
//

import UIKit

class FactsCollectionViewCell: UICollectionViewCell {
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    func setupView() {
        backgroundColor = Constants.colors.cellBackgroundColor
        addSubview(titleLabel)
        titleLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 8).isActive = true
        titleLabel.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        titleLabel.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        titleLabel.heightAnchor.constraint(lessThanOrEqualToConstant: 30).isActive = true
        
        addSubview(imageView)
        imageView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 8).isActive = true
        imageView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8).isActive = true
        imageView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -8).isActive = true
        
        addSubview(descriptionLabel)
        descriptionLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 8).isActive = true
        descriptionLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8).isActive = true
        descriptionLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -8).isActive = true
        descriptionLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -8).isActive = true
    }
    
    func fillWithData(_ data : FactObject) {
        self.titleLabel.text = data.titleString
        self.descriptionLabel.text = data.descriptionString
        
        if (data.imageHerfString != "") {
            imageView.heightAnchor.constraint(equalToConstant: 100).isActive = true
            imageView.heightAnchor.constraint(equalToConstant: 0).isActive = false
            if (data.imageData != nil) {
                imageView.image = UIImage.init(data: data.imageData!)
            } else {
                imageView.image = #imageLiteral(resourceName: "placeholder")
                
                URLSession.shared.dataTask( with: NSURL(string:data.imageHerfString)! as URL, completionHandler: {
                    (imageData, response, error) -> Void in
                    DispatchQueue.main.async {
                        if let imgdata = imageData {
                            if (imgdata.count != 0) {
                                self.imageView.image = UIImage(data: imgdata)
                                data.imageData = imgdata
                            }
                        }
                    }
                }).resume()
            }
        } else {
            imageView.heightAnchor.constraint(equalToConstant: 0).isActive = true
            imageView.heightAnchor.constraint(equalToConstant: 100).isActive = false
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let titleLabel : UILabel = {
        let label = UILabel()
        label.text = "Sample Title"
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 17)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let descriptionLabel : UILabel = {
        let label = UILabel()
        label.text = "Sample Description"
        label.font = UIFont.systemFont(ofSize: 12)
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let imageView : UIImageView = {
        let imageView = UIImageView()
        imageView.layer.cornerRadius = 5
        imageView.layer.masksToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    
}
