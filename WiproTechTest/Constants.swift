//
//  Constants.swift
//  WiproTechTest
//
//  Created by Navid Farshchi on 20/5/18.
//  Copyright © 2018 Navid Farshchi. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    
    struct server {
        static let serverURL = "https://dl.dropboxusercontent.com/s"
        static let serviceAPIURL = "/2iodh4vg0eortkl/facts.json"
    }
    
    struct colors {
        static let cellBackgroundColor = UIColor(red: (238.0/255.0), green: (238.0/255.0), blue: (238.0/255.0), alpha: 1.0)
    }
}
